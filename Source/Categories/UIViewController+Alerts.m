//
//  PRSettingsVC.h
//  Perimeter
//
//  Created by Dima Krasulya on 6/15/16.
//  Copyright © 2016 Argus Soft. All rights reserved.
//

#import "UIViewController+Alerts.h"

@implementation UIViewController (Alerts)

-(void)showAlerControllerWithTitle:(NSString *)title message:(NSString *)message
{
    UIAlertController* alert = [UIAlertController alertControllerWithTitle:title
                                                                   message:message
                                                            preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction* defaultAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault
                                                          handler:^(UIAlertAction * action) {
                                                              
                                                          }];
    
    [alert addAction:defaultAction];
    [self presentViewController:alert animated:YES completion:nil];

}


//==============================================================================


@end
