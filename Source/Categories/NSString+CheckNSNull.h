//
//  PRSettingsVC.h
//  Perimeter
//
//  Created by Dima Krasulya on 6/15/16.
//  Copyright © 2016 Argus Soft. All rights reserved.
//
//==============================================================================

#import <Foundation/Foundation.h>

@interface NSString (CheckNSNull)

+ (NSString *)checkNSNull:(id)value;

+ (NSString *)checkNil:(id)value;

@end
