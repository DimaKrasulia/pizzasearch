//
//  PRSettingsVC.h
//  Perimeter
//
//  Created by Dima Krasulya on 6/15/16.
//  Copyright © 2016 Argus Soft. All rights reserved.
//

#import "NSNumber+CheckNSNull.h"

@implementation NSNumber(CheckNSNull)

//==============================================================================


+ (NSNumber *)checkNSNull:(id)value
{
    if (value == [NSNull null] || value == nil)
        return @0;
    else
        return value;
}


//==============================================================================


- (NSString *)stringBoolean
{
    return self.boolValue ? @"TRUE" : @"FALSE";
}


//==============================================================================

@end
