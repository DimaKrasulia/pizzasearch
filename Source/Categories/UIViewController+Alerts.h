//
//  PRSettingsVC.h
//  Perimeter
//
//  Created by Dima Krasulya on 6/15/16.
//  Copyright © 2016 Argus Soft. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIViewController (Alerts)

-(void)showAlerControllerWithTitle:(NSString *)title message:(NSString *)message;


@end
