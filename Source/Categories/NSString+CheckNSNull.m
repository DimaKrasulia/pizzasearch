//
//  PRSettingsVC.h
//  Perimeter
//
//  Created by Dima Krasulya on 6/15/16.
//  Copyright © 2016 Argus Soft. All rights reserved.
//
//==============================================================================

#import "NSString+CheckNSNull.h"

@implementation NSString (CheckNSNull)

+ (NSString *)checkNSNull:(id)value
{
	if (value == [NSNull null] || value == nil)
		return @"";
    else
		return value;
}


+(NSString *)checkNil:(id)value
{
    if([value isKindOfClass:NSString.class])
        return value;
    else
        return @"";
}

@end
