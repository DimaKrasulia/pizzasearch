//
//  PRSettingsVC.h
//  Perimeter
//
//  Created by Dima Krasulya on 6/15/16.
//  Copyright © 2016 Argus Soft. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSNumber(_CheckNSNull)

/// Check if the NSNumber is NULL. Return @0 instead.
+ (NSNumber *)checkNSNull:(id)value;

- (NSString *)stringBoolean;

@end
