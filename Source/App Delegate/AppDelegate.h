//
//  AppDelegate.h
//  PizzaSearch
//
//  Created by Красуля Дмитрий on 12.11.16.
//  Copyright © 2016 Красуля Дмитрий. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreData/CoreData.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

