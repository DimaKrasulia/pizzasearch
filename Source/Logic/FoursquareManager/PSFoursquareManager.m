//
//  PSFoursquareManager.m
//  PizzaSearch
//
//  Created by Красуля Дмитрий on 12.11.16.
//  Copyright © 2016 Красуля Дмитрий. All rights reserved.
//

#import "PSFoursquareManager.h"
#import "PSPizzaPlace+CoreDataClass.h"
#import <CoreLocation/CoreLocation.h>

@interface PSFoursquareManager ()<CLLocationManagerDelegate>
{
    NSMutableArray *_observers;
}
@property(nonatomic, strong) AFHTTPSessionManager *sessionManager;
@property(nonatomic, strong) CLLocationManager *locationManager;
@property(nonatomic, strong) CLLocation *lastLocation;
@property(nonatomic, assign) CLLocationDistance distance;
@property(nonatomic, copy) PSLocationCompletion completion;
@end


static PSFoursquareManager *sharedManager;

@implementation PSFoursquareManager

+(instancetype)shared
{
    @synchronized (self)
    {
        if (sharedManager == nil)
        {
            sharedManager = [[PSFoursquareManager alloc] init];
        }
    }
    return sharedManager;
}

-(instancetype)init
{
    self = [super init];
    if (self)
    {
        _observers = [NSMutableArray array];
        [self setupSesstionManager];
        [self setupLocationManager];
    }
    return self;
}


- (void)setupLocationManager
{
    self.locationManager = [[CLLocationManager alloc] init];
    self.locationManager.distanceFilter = kCLLocationAccuracyBestForNavigation;
    self.locationManager.delegate = self;
    
    [self.locationManager requestWhenInUseAuthorization];
    
}

- (void)setupSesstionManager
{
    self.sessionManager = [[AFHTTPSessionManager alloc] initWithBaseURL:[NSURL URLWithString:kBaseURL]];
    [self.sessionManager setResponseSerializer:[AFJSONResponseSerializer serializer]];
    [self.sessionManager setRequestSerializer:[AFHTTPRequestSerializer serializer]];
    [self.sessionManager setCompletionQueue:dispatch_get_global_queue(QOS_CLASS_UTILITY, 0)];
}

-(void)cleanStorage
{
    [MagicalRecord saveWithBlock:^(NSManagedObjectContext * _Nonnull localContext) {
        for (PSPizzaPlace *place in [PSPizzaPlace MR_findAll]) {
            [place MR_deleteEntityInContext:localContext];
        }
    }];
}

- (void)startUpdateLocationWithCompletion:(PSLocationCompletion)completion
{
    [self.locationManager startUpdatingLocation];
    if (completion)
        self.completion = completion;
}

- (void)updateListOfRestrauntWithOffset:(NSInteger)offset success:(PSSuccessBlock)theSuccess failure:(PSFailureBlock)theFailure
{
    [self startUpdateLocationWithCompletion:^(CLLocation *location)
    {
        NSString *locationString = [NSString stringWithFormat:@"%f,%f",location.coordinate.latitude, location.coordinate.longitude];
    
        NSMutableDictionary *parameters = [NSMutableDictionary dictionaryWithDictionary:[self preparedParameers]];
        [parameters setValue:locationString forKey:@"ll"];
        [parameters setValue:@(offset) forKey:@"offset"];
        
        [[self sessionManager] GET:kVenueExploreEndPoint
                        parameters:parameters
                          progress:NULL
                           success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
                               
                               [self parseResponse:responseObject[kResponse] success:theSuccess failure:theFailure];
                               
                           } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
                               if (theFailure)
                                   theFailure(error);
                           }];
    }];
}


-(void)parseResponse:(NSDictionary *)response success:(PSSuccessBlock)theSuccess failure:(PSFailureBlock)theFailure

{
    
    [MagicalRecord saveWithBlock:^(NSManagedObjectContext * _Nonnull localContext) {
        if ([NSDictionary isDictionaryEmpty:response])
            return;
        NSArray *items = [response[kGroups] firstObject][kItems];
        for (NSDictionary *place in items) {
            PSPizzaPlace *entity = [PSPizzaPlace MR_findFirstOrCreateByAttribute:@"placeID" withValue:place[kVenue][kID] inContext:localContext];
            [entity updateWithDictionary:place];
        }
    } completion:^(BOOL contextDidSave, NSError * _Nullable error) {
        if (!error && theSuccess)
            theSuccess();
        else if (error && theFailure)
            theFailure(error);
    }];
}

-(NSDictionary *)preparedParameers
{
    NSDateFormatter *dateFormater = [NSDateFormatter new];
    dateFormater.dateFormat = @"YYYYMMdd";
    return  @{
              @"client_id":kClientID,
              @"client_secret":kClientSecret,
              @"query":@"pizza",
              @"limit":@10,
              @"sortByDistance":@1,
              @"v":[dateFormater stringFromDate:[NSDate date]],
              @"m":@"foursquare",
              @"venuePhotos":@1
              };
}

#pragma mark - CLLocationManagerDelegate

-(void)locationManager:(CLLocationManager *)manager didUpdateLocations:(NSArray<CLLocation *> *)locations
{
    CLLocation *myLocation = [locations lastObject];
    if (myLocation)
    {
        [manager stopUpdatingLocation];
        self.distance = [myLocation distanceFromLocation:self.lastLocation];
        [self notifyObservers];
        self.lastLocation = myLocation;
        if (self.completion)
            self.completion(self.lastLocation);
    }
    
}

#pragma mark - Observers

-(void)addObserver:(id<PSFoursquareManagerObserver>)observer
{
    [_observers addObject:observer];
}

-(void)removeObserver:(id<PSFoursquareManagerObserver>)observer
{
    [_observers removeObject:observer];
}

- (void)notifyObservers
{
    for (id<PSFoursquareManagerObserver> observer in _observers) {
        [observer locationDidChangeWithDistance:self.distance];
    }
}

@end
