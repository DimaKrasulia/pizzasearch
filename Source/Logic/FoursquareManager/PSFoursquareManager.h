//
//  PSFoursquareManager.h
//  PizzaSearch
//
//  Created by Красуля Дмитрий on 12.11.16.
//  Copyright © 2016 Красуля Дмитрий. All rights reserved.
//

#import <Foundation/Foundation.h>
@class CLLocation;

typedef void(^PSLocationCompletion)(CLLocation *location);

@protocol PSFoursquareManagerObserver <NSObject>

- (void)locationDidChangeWithDistance:(double)distance;

@end

@interface PSFoursquareManager : NSObject


+ (instancetype)shared;

- (void)updateListOfRestrauntWithOffset:(NSInteger)offset success:(PSSuccessBlock)theSuccess failure:(PSFailureBlock)theFailure;

- (void)cleanStorage;

- (void)addObserver:(id<PSFoursquareManagerObserver>)observer;
- (void)removeObserver:(id<PSFoursquareManagerObserver>)observer;

@end
