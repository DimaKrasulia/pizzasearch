
typedef void(^PSCompletion)();
typedef void(^PSSuccessBlock)();
typedef void(^PSFailureBlock)(NSError *error);

extern NSString *const kClientID;
extern NSString *const kCallBackURL;
extern NSString *const kClientSecret;
extern NSString *const kAppURLScheme;
extern NSString *const kBaseURL;
extern NSString *const kVenueExploreEndPoint;

//Foursquare keys
extern NSString *const kResponse;
extern NSString *const kGroups;
extern NSString *const kItems;
extern NSString *const kVenue;
extern NSString *const kLocation;
extern NSString *const kID;
extern NSString *const kDistance;
extern NSString *const kRating;
extern NSString *const kName;
extern NSString *const kFormattedAddress;
