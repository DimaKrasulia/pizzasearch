//
//  PSPizzaPlace+CoreDataClass.h
//  PizzaSearch
//
//  Created by Красуля Дмитрий on 13.11.16.
//  Copyright © 2016 Красуля Дмитрий. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

NS_ASSUME_NONNULL_BEGIN

@interface PSPizzaPlace : NSManagedObject

- (void)updateWithDictionary:(NSDictionary *)dictionary;

@end

NS_ASSUME_NONNULL_END

#import "PSPizzaPlace+CoreDataProperties.h"
