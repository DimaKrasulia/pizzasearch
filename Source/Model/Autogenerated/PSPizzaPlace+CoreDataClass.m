//
//  PSPizzaPlace+CoreDataClass.m
//  PizzaSearch
//
//  Created by Красуля Дмитрий on 13.11.16.
//  Copyright © 2016 Красуля Дмитрий. All rights reserved.
//

#import "PSPizzaPlace+CoreDataClass.h"

@implementation PSPizzaPlace

-(void)updateWithDictionary:(NSDictionary *)dictionary
{
    NSParameterAssert(dictionary);
    
    NSDictionary *venue = dictionary[kVenue];
    NSDictionary *location = venue[kLocation];
    self.placeID = [NSString checkNSNull:venue[kID]];
    self.name = [NSString checkNSNull:venue[kName]];
    self.distance = [NSNumber checkNSNull:location[kDistance]];
    self.rating = [NSNumber checkNSNull:venue[kRating]];
    self.formattedAddress = [location[kFormattedAddress] componentsJoinedByString:@"\n"];
}

@end
