//
//  PSPizzaPlace+CoreDataProperties.m
//  PizzaSearch
//
//  Created by Красуля Дмитрий on 13.11.16.
//  Copyright © 2016 Красуля Дмитрий. All rights reserved.
//

#import "PSPizzaPlace+CoreDataProperties.h"

@implementation PSPizzaPlace (CoreDataProperties)

+ (NSFetchRequest<PSPizzaPlace *> *)fetchRequest {
	return [[NSFetchRequest alloc] initWithEntityName:@"PSPizzaPlace"];
}

@dynamic placeID;
@dynamic name;
@dynamic distance;
@dynamic formattedAddress;
@dynamic rating;

@end
