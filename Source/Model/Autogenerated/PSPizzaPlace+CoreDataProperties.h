//
//  PSPizzaPlace+CoreDataProperties.h
//  PizzaSearch
//
//  Created by Красуля Дмитрий on 13.11.16.
//  Copyright © 2016 Красуля Дмитрий. All rights reserved.
//

#import "PSPizzaPlace+CoreDataClass.h"


NS_ASSUME_NONNULL_BEGIN

@interface PSPizzaPlace (CoreDataProperties)

+ (NSFetchRequest<PSPizzaPlace *> *)fetchRequest;

@property (nonnull, nonatomic, copy) NSString *placeID;
@property (nullable, nonatomic, copy) NSString *name;
@property (nullable, nonatomic, copy) NSNumber *distance;
@property (nullable, nonatomic, copy) NSString *formattedAddress;
@property (nullable, nonatomic, copy) NSNumber * rating;

@end

NS_ASSUME_NONNULL_END
