//
//  ViewController.m
//  PizzaSearch
//
//  Created by Красуля Дмитрий on 12.11.16.
//  Copyright © 2016 Красуля Дмитрий. All rights reserved.
//

#import "PSPlaceListViewController.h"
#import "PSPizzaPlace+CoreDataClass.h"
#import "PSPizzaPlaceCell.h"
#import "PSFoursquareManager.h"
#import "PSPlaceDetaiViewController.h"

static NSString *const kPSPlaceDetaileSegue = @"PSPlaceDetaileSegue";

@interface PSPlaceListViewController ()<UITableViewDelegate, UITableViewDataSource, NSFetchedResultsControllerDelegate, PSFoursquareManagerObserver>
{
    NSInteger _offset;
}

@property (nonatomic, weak) IBOutlet UITableView *tableView;
@property (nonatomic, strong) NSFetchedResultsController *placesFetchResultsController;

@end

@implementation PSPlaceListViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    _offset  = 0;
    
    [self.tableView registerNib:[UINib nibWithNibName:NSStringFromClass([PSPizzaPlaceCell class]) bundle:nil] forCellReuseIdentifier:NSStringFromClass([PSPizzaPlaceCell class])];
    
    self.placesFetchResultsController = [PSPizzaPlace MR_fetchAllSortedBy:kDistance ascending:YES withPredicate:nil groupBy:nil delegate:self];
    [[PSFoursquareManager shared] addObserver:self];
    __weak PSPlaceListViewController *weakSelf = self;

    [[PSFoursquareManager shared] updateListOfRestrauntWithOffset:_offset success:nil failure:^(NSError *error) {
        dispatch_async(dispatch_get_main_queue(), ^{
            __strong PSPlaceListViewController *stronSelf = weakSelf;
            [stronSelf showAlerControllerWithTitle:@"" message:error.localizedDescription];
            
        });
    }];
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self.placesFetchResultsController performFetch:nil];
    [self.tableView reloadData];
}

-(void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    [[PSFoursquareManager shared] removeObserver:self];
}


#pragma mark - UITableViewDelegate & UITableViewDataSource

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 82;
}

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return [[self.placesFetchResultsController sections] count];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    NSArray *sections = [self.placesFetchResultsController sections];
    id<NSFetchedResultsSectionInfo> sectionInfo = [sections objectAtIndex:section];
    
    return [sectionInfo numberOfObjects];
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    PSPizzaPlaceCell *cell = (PSPizzaPlaceCell *)[tableView dequeueReusableCellWithIdentifier:NSStringFromClass([PSPizzaPlaceCell class]) forIndexPath:indexPath];
    PSPizzaPlace *place = [self.placesFetchResultsController objectAtIndexPath:indexPath];
    [cell updateWithPizzaPlace:place];
    if (indexPath.section == [tableView numberOfSections]-1 && indexPath.row == [tableView numberOfRowsInSection:indexPath.section] - 1)
    {
        [cell.spinner setHidden:NO];
        [cell.spinner startAnimating];
        
        _offset = [tableView numberOfRowsInSection:indexPath.section] + 10;
        
        __weak PSPizzaPlaceCell *weakCell = cell;
        __weak PSPlaceListViewController *weakSelf = self;
        [[PSFoursquareManager shared] updateListOfRestrauntWithOffset:_offset success:^{
            dispatch_async(dispatch_get_main_queue(), ^{
                __strong PSPizzaPlaceCell *strongCell = weakCell;
                [strongCell.spinner setHidden:NO];
                [strongCell.spinner startAnimating];
                [weakSelf.tableView reloadData];
            });
            
        } failure:^(NSError *error) {
            dispatch_async(dispatch_get_main_queue(), ^{
                __strong PSPizzaPlaceCell *strongCell = weakCell;
                __strong PSPlaceListViewController *strongSelf = weakSelf;
                
                [strongSelf showAlerControllerWithTitle:@"" message:error.localizedDescription];
                [strongCell.spinner setHidden:YES];
                [strongCell.spinner stopAnimating];
            });
        }];
    }
    else
        [cell.spinner setHidden:YES];
    
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    PSPizzaPlace *place = [self.placesFetchResultsController objectAtIndexPath:indexPath];
    [self performSegueWithIdentifier:kPSPlaceDetaileSegue sender:place];
    
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
}

#pragma mark - NSFetchedResultsControllerDelegate

- (void)controllerWillChangeContent:(NSFetchedResultsController *)controller {
    [self.tableView beginUpdates];
}

- (void)controllerDidChangeContent:(NSFetchedResultsController *)controller {
    [self.tableView endUpdates];
}

- (void)controller:(NSFetchedResultsController *)controller didChangeObject:(id)anObject atIndexPath:(NSIndexPath *)indexPath forChangeType:(NSFetchedResultsChangeType)type newIndexPath:(NSIndexPath *)newIndexPath {
    switch (type) {
        case NSFetchedResultsChangeInsert: {
            
            [self.tableView insertRowsAtIndexPaths:[NSArray arrayWithObject:newIndexPath] withRowAnimation:UITableViewRowAnimationFade];
            
            break;
        }
        case NSFetchedResultsChangeDelete: {
            [self.tableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationFade];
            break;
        }
        case NSFetchedResultsChangeUpdate: {
            [self.tableView reloadRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewAutomaticDimension];
            break;
        }
        case NSFetchedResultsChangeMove: {
            [self.tableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationFade];
            [self.tableView insertRowsAtIndexPaths:[NSArray arrayWithObject:newIndexPath] withRowAnimation:UITableViewRowAnimationFade];
            break;
        }
    }
}

#pragma mark - PSFoursquareManagerObserver

-(void)locationDidChangeWithDistance:(double)distance
{
    /*Premitive logic for location changes. */
    
    if (distance > 1000) {
        _offset = 0;
         __weak PSPlaceListViewController *weakSelf = self;
        [[PSFoursquareManager shared] cleanStorage];
        [[PSFoursquareManager shared] updateListOfRestrauntWithOffset:_offset success:nil failure:^(NSError *error) {
            dispatch_async(dispatch_get_main_queue(), ^{
                __strong PSPlaceListViewController *stronSelf = weakSelf;
                [stronSelf showAlerControllerWithTitle:@"" message:error.localizedDescription];
            });
        }];
    }
}


- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
  
    if ([segue.identifier isEqualToString:kPSPlaceDetaileSegue]) {
        PSPizzaPlace *place = (PSPizzaPlace *)sender;
        PSPlaceDetaiViewController *vc = (PSPlaceDetaiViewController *)segue.destinationViewController;
        vc.pizzaPlace = place;
    }

}

@end
