//
//  PSPizzaPlaceCell.h
//  PizzaSearch
//
//  Created by Красуля Дмитрий on 13.11.16.
//  Copyright © 2016 Красуля Дмитрий. All rights reserved.
//

#import <UIKit/UIKit.h>


@class PSPizzaPlace;

@interface PSPizzaPlaceCell : UITableViewCell

@property (nonatomic, weak) IBOutlet UILabel *name;
@property (nonatomic, weak) IBOutlet UILabel *distance;
@property (nonatomic, weak) IBOutlet UIActivityIndicatorView *spinner;

- (void)updateWithPizzaPlace:(PSPizzaPlace *)pizzaPlace;

@end
