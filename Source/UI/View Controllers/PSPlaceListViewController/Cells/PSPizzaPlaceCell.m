//
//  PSPizzaPlaceCell.m
//  PizzaSearch
//
//  Created by Красуля Дмитрий on 13.11.16.
//  Copyright © 2016 Красуля Дмитрий. All rights reserved.
//

#import "PSPizzaPlaceCell.h"
#import "PSPizzaPlace+CoreDataClass.h"

@implementation PSPizzaPlaceCell

-(void)prepareForReuse
{
    [super prepareForReuse];
    [self.name setText:@""];
    [self.distance setText:@""];
    
}

-(void)updateWithPizzaPlace:(PSPizzaPlace *)pizzaPlace
{
    self.name.text = pizzaPlace.name;
    self.distance.text = [NSString stringWithFormat:@"%@ meters",pizzaPlace.distance];
}

@end
