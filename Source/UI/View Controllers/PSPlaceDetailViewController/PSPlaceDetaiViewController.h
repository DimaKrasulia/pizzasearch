//
//  PSPlaceDetaiViewController.h
//  PizzaSearch
//
//  Created by Красуля Дмитрий on 13.11.16.
//  Copyright © 2016 Красуля Дмитрий. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PSPizzaPlace+CoreDataClass.h"

@interface PSPlaceDetaiViewController : UIViewController

@property (strong, nonatomic) PSPizzaPlace *pizzaPlace;

@end
