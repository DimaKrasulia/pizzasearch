//
//  PSPlaceDetaiViewController.m
//  PizzaSearch
//
//  Created by Красуля Дмитрий on 13.11.16.
//  Copyright © 2016 Красуля Дмитрий. All rights reserved.
//

#import "PSPlaceDetaiViewController.h"

@interface PSPlaceDetaiViewController ()

@property (nonatomic, weak) IBOutlet UILabel *nameLabel;
@property (nonatomic, weak) IBOutlet UILabel *ratingLabel;
@property (nonatomic, weak) IBOutlet UILabel *addressLabel;
@property (nonatomic, weak) IBOutlet UILabel *distanceLabel;
@property (nonatomic, weak) IBOutlet NSLayoutConstraint *addressLableHeight;

@end

@implementation PSPlaceDetaiViewController

-(void)viewWillLayoutSubviews
{
    [super viewWillLayoutSubviews];
    self.addressLableHeight.constant = [self heightWithTitleText:self.pizzaPlace.formattedAddress];
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"Back" style:UIBarButtonItemStylePlain target:self action:@selector(backButtonPressed)];
    
    [self setupLabels];
}

- (void)backButtonPressed
{
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)setupLabels
{
    self.nameLabel.text = self.pizzaPlace.name;
    self.addressLabel.text = self.pizzaPlace.formattedAddress;
    self.ratingLabel.text = [NSString stringWithFormat:@"Rating %.1f",self.pizzaPlace.rating.floatValue];
    self.distanceLabel.text = [NSString stringWithFormat:@"%@ meters",self.pizzaPlace.distance];
}

- (CGFloat)heightWithTitleText:(NSString *)theString
{
    CGSize constraint = CGSizeMake([[self addressLabel] frame].size.width, CGFLOAT_MAX);
    
    NSStringDrawingContext *context = [[NSStringDrawingContext alloc] init];
    CGSize boundingBox = [theString boundingRectWithSize:constraint
                                                 options:NSStringDrawingUsesLineFragmentOrigin
                                              attributes:@{ NSFontAttributeName: [[self addressLabel] font] }
                                                 context:context].size;
    
    CGSize size = CGSizeMake(ceil(boundingBox.width), ceil(boundingBox.height));
    
    return size.height;
}




@end
